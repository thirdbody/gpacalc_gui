# NBPS GPA Calculator

Calculates your GPA using the NBPS weighting system.

My source for the weighting system can be found [here](https://sites.google.com/a/mynbps.org/nbps-overview-course-handbook-description/gpa-and-weighting).

## Download

[Here](http://latest.gpa.milogilad.com) you go.

## Problems?

If you've got any issues, feel free to contact me at [myl0gcontact@gmail.com](mailto:myl0gcontact@gmail.com) or open an [issue](https://gitlab.com/Myl0g/gpacalc_gui/issues).
