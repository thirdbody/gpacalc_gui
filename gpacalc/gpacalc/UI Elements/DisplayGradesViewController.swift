//
//  DisplayGradesViewController.swift
//  gpacalc
//
//  Created by Milo Gilad on 10/17/18.
//  Copyright © 2018 NBPS. All rights reserved.
//

import Cocoa

final class DisplayGradesViewController: NSViewController {

    @IBOutlet weak var gradeDisplayLabel: NSTextField!
    @IBOutlet weak var gpaDisplayLabel: NSTextField!
    @IBOutlet weak var unweightedLabel: NSTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
    override func viewDidAppear() {
        super.viewDidAppear();
        
        gradebook.displayGrades(label: gradeDisplayLabel)
        
        unweightedLabel.stringValue = "Your unweighted GPA is \(gradebook.unweightedGPA)"
        gpaDisplayLabel.stringValue = "Your weighted GPA is \(gradebook.GPA)"
    }
    
}
