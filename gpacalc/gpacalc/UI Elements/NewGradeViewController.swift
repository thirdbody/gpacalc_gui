//
//  NewGradeViewController.swift
//  gpacalc
//
//  Created by Milo Gilad on 10/18/18.
//  Copyright © 2018 NBPS. All rights reserved.
//

import Cocoa

final class NewGradeViewController: NSViewController {

    @IBOutlet weak var nameField: NSTextField!
    @IBOutlet weak var letterChooser: NSPopUpButton!
    @IBOutlet weak var symbolChooser: NSPopUpButton!
    @IBOutlet weak var typeChooser: NSPopUpButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
    override func viewDidAppear() {
        super.viewDidAppear()
        
        letterChooser.removeAllItems();
        symbolChooser.removeAllItems();
        typeChooser.removeAllItems();
        
        letterChooser.addItem(withTitle: "A")
        letterChooser.addItem(withTitle: "B")
        letterChooser.addItem(withTitle: "C")
        letterChooser.addItem(withTitle: "D")
        letterChooser.addItem(withTitle: "F")
        
        symbolChooser.addItem(withTitle: "+")
        symbolChooser.addItem(withTitle: "-")
        symbolChooser.addItem(withTitle: "None")
        
        typeChooser.addItem(withTitle: "CP")
        typeChooser.addItem(withTitle: "Honors")
        typeChooser.addItem(withTitle: "Accelerated")
    }
    
    @IBAction func submit(_ sender: NSButton) {
        gradebook.addGrade(Grade(name: nameField.stringValue, letter: letterChooser.titleOfSelectedItem!, symbol: symbolChooser.titleOfSelectedItem!, type: typeChooser.titleOfSelectedItem!))
        gradebook.save();
    }
}
