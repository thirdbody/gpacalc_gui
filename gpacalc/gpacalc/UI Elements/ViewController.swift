//
//  ViewController.swift
//  gpacalc
//
//  Created by Milo Gilad on 10/17/18.
//  Copyright © 2018 NBPS. All rights reserved.
//

import Cocoa

final class ViewController: NSViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear() {
        /// Checking for updates...
        if !fileExists("\(NSHomeDirectory())/Documents/DoNotUpdate") && !isUpToDate() {
            let alert = NSAlert()
            alert.messageText = "A new update is available!"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "Download")
            alert.addButton(withTitle: "Remind Me Later")
            alert.addButton(withTitle: "Remind Me Never")
            
            let result = alert.runModal()
            if result == NSApplication.ModalResponse.alertThirdButtonReturn {
                FileManager.default.createFile(atPath: "\(NSHomeDirectory())/Documents/DoNotUpdate", contents: nil, attributes: nil)
            } else if result == NSApplication.ModalResponse.alertFirstButtonReturn {
                NSWorkspace.shared.open(URL(string: "http://latest.gpa.milogilad.com")!);
            }
            
            alert.window.close();
            NSApp.activate(ignoringOtherApps: true);
        }
        
        if prefsExist() {
            gradebook = Gradebook.load();
        } else {
            makePrefs();
        }
        performSegue(withIdentifier: "AddGradesSegue", sender: nil)
    }
    
    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

