//
//  DeleteCoursesViewController.swift
//  gpacalc
//
//  Created by Milo Gilad on 10/18/18.
//  Copyright © 2018 NBPS. All rights reserved.
//

import Cocoa

final class DeleteCoursesViewController: NSViewController {
    @IBOutlet weak var coursesPopUp: NSPopUpButton!
    
    override func viewDidAppear() {
        coursesPopUp.removeAllItems();
        
        for grade in gradebook.grades {
            coursesPopUp.addItem(withTitle: grade.courseName);
        }
    }
    
    
    @IBAction func deleteCourse(_ sender: NSButton) {
        gradebook.deleteCourse(name: coursesPopUp.titleOfSelectedItem!)
        self.viewDidAppear();
    }
}
