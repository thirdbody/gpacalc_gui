//
//  Gradebook.swift
//  gpacalc
//
//  Created by Milo Gilad on 10/17/18.
//  Copyright © 2018 NBPS. All rights reserved.
//

import Foundation
import Cocoa

/**
 Gradebook is a collection of Grades with extra features:
 
 * Calculate GPA using entire collection of Grades
 * Mass-serialize all grades in JSON format for incredibly quick and easy store/retrieval of grades
 * Ability to print Grades to a NSTextField for easy displayment on front-end
 
 Gradebook implements Codable so that serialization is possible.
*/

final public class Gradebook: Codable {
    var grades: [Grade];
    
    init(grades: [Grade]) {
        self.grades = grades;
    }
    init() {
        self.grades = [];
    }
    
    public var GPA: Double {
        var undivided = 0.0;
        
        for grade in grades {
            var gradeWorth = grade.letter;
            gradeWorth += grade.symbol; // Apply symbol
            gradeWorth *= grade.type; // Apply weight
            undivided += gradeWorth;
        }
        let result = undivided / Double(grades.count);
        return Double(round(100000*result) / 100000); // rounded to 5 places
    }
    
    public var unweightedGPA: Double {
        var undivided = 0.0;
        
        for grade in grades {
            var gradeWorth = grade.letter;
            if (grade.letterReadable + grade.symbolReadable) != "A+" {
                gradeWorth += grade.symbol; // Apply symbol
            }
            undivided += gradeWorth;
        }
        
        let result = undivided / Double(grades.count);
        return Double(round(100000*result) / 100000); // rounded to 5 places
    }
    
    public func save() {
        do {
            let toFile = try JSONEncoder().encode(self)
            writeSandboxedFile(name: "gradebook.json", data: toFile);
        } catch {
            print(error);
        }
    }
    public static func load() -> Gradebook {
        let fromFile = readSandboxedFile(name: "gradebook.json")
        if fromFile == nil { // Nothing in file; gradebook empty
            return Gradebook();
        }
        
        do {
            return try JSONDecoder().decode(Gradebook.self, from: fromFile!)
        } catch {
            print(error);
            return Gradebook();
        }
    }
    public func addGrade(_ grade: Grade) {
        self.grades.append(grade);
    }
    
    public func displayGrades(label: NSTextField) {
        var labelText = "";
        
        for grade in gradebook.grades {
            labelText += grade.description + "\n"
        }
        label.stringValue = labelText;
    }
    
    public func deleteCourse(name: String) {
        grades.remove(at: findCourseIndex(name: name)!)
        self.save();
    }
    
    public func findCourseIndex(name: String) -> Int? {
        for i in 0..<grades.count {
            if grades[i].courseName == name {
                return i;
            }
        }
        return nil;
    }
}
